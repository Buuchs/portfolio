import Dot from './dot'
let c = ""
let context= ""
let mouse = {x: 0, y: 0}
let mouseMoved = false
let mouseOver = false
let signs = []
let gridLengthX = 12
let gridLengthY = 6
let that = ""

let Grid = {
 beginGrid(canvas) {
   that = this
   c = canvas
   c.width = window.innerWidth
   c.height = window.innerHeight
   context = c.getContext("2d")

   for (let i = 0; i < gridLengthX; i++) {
    signs[i] = []
    for (let j = 0; j < gridLengthY; j++) {
      let sign = new Dot()
      sign.left = canvas.width/(gridLengthX + 1) * (i + 1)
      sign.top = canvas.height/(gridLengthY + 1) * (j + 1)
      signs[i][j] = sign
    }
   }
   
   TweenMax.ticker.addEventListener('tick', this.draw)
   
   c.addEventListener('mousemove', this.mouseMove)
   c.addEventListener('mouseleave', function() {
     mouseOver = false
     for (let i = 0; i < gridLengthX; i++) {
       for (let j = 0; j < gridLengthY; j++) {
         let sign = signs[i][j]
         TweenMax.to(sign, 0,3, {x: 0, y: 0, scale: 1})
       }
     }
   })
   c.addEventListener('mouseenter', function() {
     mouseOver = true
   })
 },

 draw() {
  context.clearRect(0, 0, c.width, c.height)

  if(mouseOver && mouseMoved) {
    that.calculateDotPosition()
    mouseMoved = false
  }
  context.save()
  
  for (let i = 0; i < gridLengthX; i++) {
    for (let j = 0; j < gridLengthY; j++) {
      let sign = signs[i][j]
      sign.draw(context)
    }
  }
  context.restore()
 },

 calculateDotPosition() {
   for (let i = 0; i < gridLengthX; i++) {
     for (let j = 0; j < gridLengthY; j++) {
       let sign = signs[i][j]
       let radius = 20
       let dx = mouse.x - sign.left
       let dy = mouse.y - sign.top
       var dist = Math.sqrt(dx * dx + dy * dy) || 1
       let angle = Math.atan2(dy, dx)
       if(dist < radius) {
         radius = dist
         TweenMax.to(sign, 0.3, {scale: 1.5})
       } else {
         TweenMax.to(sign, 0.3, {scale: 1})
       }
       TweenMax.to(sign, 0.3, { x: Math.cos(angle) * radius, y: Math.sin(angle) * radius})
     }
   }
 },

 mouseMove(e) {
   let c = document.getElementById("c")
   let rect = c.getBoundingClientRect()
   mouse.x = e.clientX - rect.left
   mouse.y = e.clientY - rect.top
   mouseMoved = true
 },

}

export default Grid