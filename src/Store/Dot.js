class Dot {
    constructor() {
      this.x = 0
      this.y = 0
    
      this.top = 0
      this.left = 0
    
      this.height = 10
      this.width = 10
      this.scale = 0.5

      this.draw = function(context) {
        context.setTransform(this.scale, 0, 0, this.scale, this.left + this.x, this.top + this.y)
        context.beginPath()
        context.arc(this.x, this.y, 1, 0, Math.PI * 2, false)
        context.strokeStyle = '#fff';
        context.stroke()
  context.closePath()
      }
    }
  }

  export default Dot